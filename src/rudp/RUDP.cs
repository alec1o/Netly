﻿namespace Netly
{
    /// <summary>
    ///     RUDP Implementation
    /// </summary>
    public static partial class RUDP
    {
        private const byte ConstPingByte = 0;
        private const uint ConstConnectionTimeout = 3000;
        private const uint ConstPingMessagePerSecond = 10;
        
        /// <summary>
        ///     RUDP Server implementation
        /// </summary>
        public partial class Server
        {
        }

        /// <summary>
        ///     RUDP Client implementation
        /// </summary>
        public partial class Client
        {
        }
    }
}